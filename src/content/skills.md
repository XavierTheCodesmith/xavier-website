---
id: skills
slug: skills/
heading: My Skills
blurbs:
    - alt: javascript
      exp: 3+ years.
      image: ../assets/img/icon-javascript.svg
      name: JavaScript
    - alt: reactjs
      exp: 3+ years
      image: ../assets/img/icon-react.svg
      name: ReactJS
    - alt: typescript
      exp: 2+ years.
      image: ../assets/img/icon-typescript.svg
      name: TypeScript
    - alt: redux
      exp: 2+ years.
      image: ../assets/img/icon-redux.svg
      name: Redux
    - alt: sass
      exp: 2+ years.
      image: ../assets/img/icon-sass.svg
      name: Sass
    - alt: firebase
      exp: 2+ years.
      image: ../assets/img/icon-firebase.svg
      name: Firebase
    - alt: nodejs
      exp: 3+ years.
      image: ../assets/img/icon-nodejs.svg
      name: NodeJS
    - alt: graphql
      exp: 1+ years.
      image: ../assets/img/icon-graphql.svg
      name: Graphql
    - alt: vuejs
      exp: 1+ years.
      image: ../assets/img/icon-vuejs.svg
      name: VueJS
    - alt: jest
      exp: 1+ years.
      image: ../assets/img/icon-jest.svg
      name: Jest
---
---
id: home
slug: home/
heading: Xavier Munroe
subheading: Frontend Web Developer
links:
    - name: About
      path: about/
    - name: Skills
      path: skills/
    - name: Portfolio
      path: portfolio/
---
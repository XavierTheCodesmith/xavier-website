---
id: portfolio
heading: My Portfolio
blurbs:
  - title: XJM Compliments
    description: Enter your name, and receive unlimted compliments courtesy of myself and https://complimentr.com/. Built using NuxtJS and deployed on Netlify.
    link: https://xjm-compliments.netlify.app/
    image: ../assets/img/portfolio-compliments-app.png
    alt: compliments app
  - title: XJM Hangman
    description: 'Classic Hangman game using HTML, JavaScript and CSS. Word API can be found here: https://github.com/mcnaveen/Random-Words-API'
    link: https://xjm-hangman.netlify.app/
    image: ../assets/img/portfolio-hangman.png
    alt: hangman app
---

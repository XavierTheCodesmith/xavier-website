---
id: about
slug: about/
---

### Who Am I?

<p class="about-content">I am a frontend web developer, former bootcamp graduate and basketball junkie born and raised in the Bronx, New York City! If you want to reach out, shoot me an email at munroexavier@gmail.com. Also check out/follow my <a href="https://github.com/XavierC4Q" target="_blank">Github</a> to see what I have been hacking on.</p>

import "~/assets/index.scss";
import DefaultLayout from "~/layouts/Default.vue";

export default function(Vue, { head }) {
  // Set default layout as a global component
  Vue.component("Layout", DefaultLayout);

  //Add Roboto font
  head.link.push({
    rel: "stylesheet",
    href:
      "https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;1,400;1,500&display=swap",
  });
}

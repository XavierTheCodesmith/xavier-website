const path = require("path");

function addStyleResource(rule) {
  rule
    .use("style-resource")
    .loader("style-resources-loader")
    .options({
      patterns: [path.resolve(__dirname, "./src/assets/**/*.scss")],
    });
}

module.exports = {
  siteName: "Xavier Munroe Website",
  siteUrl: process.env.GRIDSOME_BASE_URL,
  plugins: [
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Home",
        baseDir: "./src/content",
        path: "home.md",
      },
    },
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "About",
        baseDir: "./src/content",
        path: "about.md",
      },
    },
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Skills",
        baseDir: "./src/content",
        path: "skills.md",
      },
    },
    {
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Portfolio",
        baseDir: "./src/content",
        path: "portfolio.md",
      },
    },
  ],
  templates: {
    Home: "/",
    About: "/about/",
    Skills: "/skills/",
    Portfolio: "/portfolio/"
  },
  chainWebpack(config) {
    const types = ["vue-modules", "vue", "normal-modules", "normal"];

    types.forEach((type) => {
      addStyleResource(config.module.rule("scss").oneOf(type));
    });
  },
};
